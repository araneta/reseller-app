<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Mappers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Description of MapperProvider
 *
 * @author aldo
 */
class MapperProvider implements ServiceProviderInterface {

    public function register(Container $container) {
	$container['userMapper'] = function($c) {
	    return new MySqlMappers\UserMapper($c['pdo_adapter']);
	};
	$container['productMapper'] = function($c) {
	    return new MySqlMappers\ProductMapper($c['pdo_adapter']);
	};
    }

}
