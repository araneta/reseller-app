<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
/**
 * Description of ServiceProvider
 *
 * @author aldo
 */
class ServiceProvider implements ServiceProviderInterface {
    public function register(Container $container)
    {
        // register some services and parameters
        // on $pimple
	
	$container['userService'] = function ($c) {
	    return new UserService($c);
	};
	$container['hybridAuth'] = function ($c) {
	    $config = dirname(__FILE__) . '/hybridauth.php';
	    //var_dump(require_once $config);
	    $hybridauth = new Hybrid_Auth($config);
	    return $hybridauth;
	};
	$container['mailerService'] = function ($c) {
	    $config = require_once dirname(__FILE__) . '/smtp.php';
	    return new MailerService($c, $config);
	};
	$container['productService'] = function ($c) {
	    return new ProductService($c);
	};
	$container['mediaService'] = function ($c) {
	    return new MediaService($c);
	};

    }
}
