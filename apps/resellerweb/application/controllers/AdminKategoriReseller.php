<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//use Hybrid_Endpoint;
use Core\Services\ServiceException;
use Core\Entities\DataTablesPaging;

use ResellerApp\Entities\KategoriReseller;

class AdminKategoriReseller extends AdminUserController {

    public function __construct() {
	parent::__construct();
    }

    //put your code here
    public function index(){
	$this->data['v'] = 'admin/kategori-reseller/index';
        $this->data['meta_title'] = 'Daftar Kategori Reseller';
        $this->data['meta_desc'] = 'Daftar Kategori Reseller';
        $this->data['slug'] = 'kategori-reseller';
        $this->data['menu'] = array('url' => $this->data['slug'] , 'display' => 'Daftar Kategori Reseller');
	$this->data['jsfiles'] = array('admin-kategori-reseller.js');
        $this->load->view('template_admin', $this->data);
    }
    
    public function add($id = 0){
	$entity = new KategoriReseller();
	if($this->isPost()){
	    
	    $entity->bind($_POST);
	    $entity->id = $id==0 ? NULL : $id;
	    
	    if($entity->validate()){
		$svc = $this->container['adminService'];
                try{                    
                    $ret = $svc->saveKategoriReseller($entity);
                    if($ret){
                        $this->session->set_flashdata('success','Kategori berhasil disimpan');
                        return redirect('/admin/kategori-reseller/','refresh');
                    }else{
                        $this->session->set_flashdata('error', 'Failed to save product');
                    }

                }catch(ServiceException $e){
                    $this->session->set_flashdata('error', $e->getMessage());
                }
	    }else{
		$this->session->set_flashdata('error', $entity->error_messages());
	    }
	}else{
            if($id!==0){
                $svc = $this->container['adminService'];
                $entity = $svc->findKategoriResellerById($id);
                if($entity==NULL){
                    $this->session->set_flashdata('success','Kategori tidak ditemukan');
                    return redirect('/admin/kategori-reseller/','refresh');
                }
            }
        }
	
	$this->data['slug'] = 'kategori-reseller';      
	if($id==0){
            $this->data['meta_title'] = 'Input Kategori Reseller';
	    $this->data['meta_desc'] = 'Input Kategori Reseller';            
            $this->data['menu'] = array('url' => $this->data['slug'] , 'display' => 'Tambah Kategori');
        }else{
            $this->data['meta_title'] = 'Edit Product';
            $this->data['meta_desc'] = 'Edit Product';            
            $this->data['menu'] = array('url' => $this->data['slug'] , 'display' => 'Edit Kategori');
        }
	$this->data['entity'] = $entity;
	$this->data['v'] = 'admin/kategori-reseller/add';        
          
	$this->data['jsfiles'] = array('admin-kategori-reseller.js');
        $this->load->view('template_admin', $this->data);
    }
    public function delete(){
        $entity = new KategoriReseller();
	if($this->isPost()){
	    $entity->bind($_POST);	    
	    $svc = $this->container['adminService'];
	    try{
		$ret = $svc->deleteKategoriReseller($entity);
		if($ret){
		    echo 'Kategori berhasil dihapus';
		}else{
		    echo 'Kategori gagal dihapus';
		}
	    }catch(ServiceException $e){
		echo $e->getMessage();
	    }
	}else{
	    echo 'POST only';
	}
    }
    /*ajax*/
    public function search_ajax(){
        $form = new DataTablesPaging();
        $cols = ['id','nama','tipe'];
        $form->setValidColumns($cols);

        $form->bind($_POST);
        if(!$form->validate()){
            echo 'error bind paging'.$form->error_messages();
        }else{

            $this->load->helper('datatables');

            $svc = $this->container['adminService'];
            $result = $svc->searchKategoriReseller($form);
            //echo json_encode($result);
            datatables_json($result,$cols);
        }
    }
}
