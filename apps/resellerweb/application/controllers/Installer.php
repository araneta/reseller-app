<?php

defined('BASEPATH') OR exit('No direct script access allowed');


use Core\Entities\LoginForm;
use Core\Entities\ForgotForm;
use Core\Entities\ResetPasswordForm;
//use Hybrid_Endpoint;
use Core\Services\ServiceException;


use ResellerApp\Entities\AdminUser;
class Installer extends MY_Controller {

    public function __construct() {
	parent::__construct();
    }
    
    public function create_admin(){
	$admin = new AdminUser();
	$admin->firstName = 'admin';
	$admin->lastName = 'admin';
	$admin->email = 'admin@aldoapp.com';
	$admin->password = 'P@ssw0rd';
	
	$svc = $this->container['userService'];
	try {
	    $user = $svc->registerUser($admin);
	    if ($user) {
		echo 'admin user created';
	    } else {
		echo 'failed to create admin user';
	    }
	} catch (ServiceException $e) {
	    echo $e->getMessage();
	}
		
    }
    
}