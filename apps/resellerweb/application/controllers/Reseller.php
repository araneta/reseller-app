<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Core\Entities\User as UserEntity;
use Core\Entities\LoginForm;
use Core\Entities\ForgotForm;
use Core\Entities\ResetPasswordForm;
//use Hybrid_Endpoint;
use Core\Services\ServiceException;

class Reseller extends MY_Controller {

    public function __construct() {
	parent::__construct();
    }

    private function redirectIfLoggedIn() {
	if ($this->check_role('RESELLER')) {
	    $this->session->set_flashdata('success', 'Welcome');
	    redirect('/reseller/', 'refresh');
	    exit(0);
	}
    }
    public function index(){
	if (!$this->check_role('RESELLER')) {
	    $this->session->set_flashdata('error', 'Please login');
	    redirect('/reseller/login', 'refresh');
	    exit(0);
	}
	$this->data['v'] = 'reseller/index';
        $this->data['meta_title'] = 'Dashboard';
        $this->data['meta_desc'] = 'Dashboard';
        $this->data['slug'] = 'dashboard';
        $this->data['menu'] = array('url' => $this->data['slug'] , 'display' => 'Dashboard');

        $this->load->view('template_reseller', $this->data);
    }
    //signin
    public function login() {
	$form = new LoginForm();
	if ($this->isPost()) {
	    $form->bind($_POST);
	    if ($form->validate()) {
		$svc = $this->container['userService'];
		try {
		    $user = $svc->login($form->username, $form->password);
		    if ($user) {
			$this->set_user($user);

			$this->session->set_flashdata('success', 'You have successfully signed in');
			return redirect('/reseller', 'refresh');
		    } else {
			$this->session->set_flashdata('error', 'Failed to login');
		    }
		} catch (ServiceException $e) {
		    $this->session->set_flashdata('error', $e->getMessage());
		}
	    } else {
		$this->session->set_flashdata('error', $form->error_messages());
	    }
	    $this->data['user'] = $form;
	} else {
	    $this->redirectIfLoggedIn();
	    $this->data['user'] = $form;
	}

	$this->data['v'] = 'reseller/signin';
	$this->data['meta_title'] = 'Signin';
	$this->data['meta_desc'] = 'Signin';
	$this->data['slug'] = 'signin';

	$this->data['menu'] = array('url' => $this->data['slug'], 'display' => 'Signin');
	$this->load->view('template_guest', $this->data);
    }
    //unused
    private function signup() {
	$form = new UserEntity();
	if ($this->isPost()) {
	    $form->bind($_POST);
	    if ($form->validate()) {
		$svc = $this->container['userService'];
		try {
		    $ret = $svc->registerUser($form);
		    if ($ret) {
			$this->session->set_flashdata('success', 'You have successfully registered');
			return redirect('/reseller', 'refresh');
		    } else {
			$this->session->set_flashdata('error', 'Failed to save user');
		    }
		} catch (ServiceException $e) {
		    $this->session->set_flashdata('error', $e->getMessage());
		}
	    } else {
		$this->session->set_flashdata('error', $form->error_messages());
	    }
	    $this->data['user'] = $form;
	} else {
	    $this->redirectIfLoggedIn();
	    $this->data['user'] = $form;
	}

	$this->data['v'] = 'reseller/signup';
	$this->data['meta_title'] = 'Signup';
	$this->data['meta_desc'] = 'Signup';
	$this->data['slug'] = 'signup';

	$this->data['menu'] = array('url' => $this->data['slug'], 'display' => 'Signup');
	$this->load->view('template_guest', $this->data);
    }

    public function logout() {
	$this->unset_user();
	$this->session->set_flashdata('success', "You've been logged out!");
	redirect('/reseller/', 'refresh');
    }

    public function forgot() {
	$form = new ForgotForm();
	if ($this->isPost()) {
	    $form->bind($_POST);
	    if ($form->validate()) {
		$svc = $this->container['userService'];
		try {
		    $ret = $svc->tryToResetPassword($form->username);
		    if ($ret) {
			$this->session->set_flashdata('success', 'Please check your email to reset the password');
			return redirect('/reseller/forgot', 'refresh');
		    } else {
			$this->session->set_flashdata('error', 'Failed to reset password');
		    }
		} catch (ServiceException $e) {
		    $this->session->set_flashdata('error', $e->getMessage());
		}
	    } else {
		$this->session->set_flashdata('error', $form->error_messages());
	    }
	    $this->data['user'] = $form;
	} else {
	    $this->redirectIfLoggedIn();
	    $this->data['user'] = $form;
	}

	$this->data['v'] = 'reseller/forgot';
	$this->data['meta_title'] = 'Forgot Password';
	$this->data['meta_desc'] = 'Forgot Password';
	$this->data['slug'] = 'forgot-password';

	$this->data['menu'] = array('url' => $this->data['slug'], 'display' => 'Forgot Password');
	$this->load->view('template_guest', $this->data);
    }

    public function reset($userId, $forgotCode) {
	$form = new ResetPasswordForm();

	if ($this->isPost()) {
	    $form->bind($_POST);
	    if ($form->validate()) {
		$svc = $this->container['userService'];
		try {
		    $ret = $svc->resetPassword($form);
		    if ($ret) {
			$this->session->set_flashdata('success', 'Password updated. Please login');
			return redirect('/reseller', 'refresh');
		    } else {
			$this->session->set_flashdata('error', 'Failed to update the passsword');
		    }
		} catch (ServiceException $e) {
		    $this->session->set_flashdata('error', $e->getMessage());
		}
	    } else {
		$this->session->set_flashdata('error', $form->error_messages());
	    }
	    $this->data['user'] = $form;
	} else {
	    $this->redirectIfLoggedIn();

	    if (empty($userId) || empty($forgotCode)) {
		$this->session->set_flashdata('error', 'Invalid user id / forgot password code');
		return redirect('/reseller/forgot', 'refresh');
	    }

	    $form->userId = $userId;
	    $form->forgotPasswordCode = $forgotCode;

	    $svc = $this->container['userService'];

	    $user = $svc->findByIdAndForgotPasswordCode($form->userId, $form->forgotPasswordCode);

	    if ($user == NULL) {
		$this->session->set_flashdata('error', 'Invalid user id / forgot password code');
		return redirect('/reseller/forgot', 'refresh');
	    }

	    $this->data['user'] = $form;
	}

	$this->data['v'] = 'reseller/reset';
	$this->data['meta_title'] = 'Reset Password';
	$this->data['meta_desc'] = 'Reset Password';
	$this->data['slug'] = 'reset-password';

	$this->data['menu'] = array('url' => $this->data['slug'], 'display' => 'Reset Password');
	$this->load->view('template_guest', $this->data);
    }
    public function profile(){
        if($this->isPost()){
            $form = new UserEntity();
            $form->bind($_POST);
            if($form->validate()) {
                $svc = $this->container['userService'];
                try{
                    $ret = $svc->updateUser($form);
                    if($ret){
                        $this->session->set_flashdata('success','You have successfully updated your profile');
                        return redirect('/reseller/profile','refresh');
                    }else{
                        $this->session->set_flashdata('error', 'Failed to update profile');
                    }
                }catch(ServiceException $e){
                    $this->session->set_flashdata('error', $e->getMessage());
                }
            }else{
                $this->session->set_flashdata('error', $form->error_messages());
            }
            $this->data['user'] = $form;
        }else{
            $svc = $this->container['userService'];
            $user = $svc->findById($this->get_user_id());

            if($user==NULL){
                $this->session->set_flashdata('error','User not found');
                return redirect('/reseller/logout','refresh');
            }
            $this->data['user'] = $user;
        }
        if($this->check_role('SOCIAL_USER')){
            $this->data['v'] = 'reseller/social-profile';
        }else{
            $this->data['v'] = 'reseller/profile';
        }

        $this->data['meta_title'] = 'Profile';
        $this->data['meta_desc'] = 'Profile';
        $this->data['slug'] = 'profile';
        $this->data['menu'] = array('url' => $this->data['slug'] , 'display' => 'Profile');

        $this->load->view('template_reseller', $this->data);
    }

    /* social logins */

    public function social_signup_callback() {
	if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done'])) {
	    Hybrid_Endpoint::process();
	}
    }

    private function hybridauth_login($provider) {
	try {
	    // create an instance for Hybridauth with the configuration file path as parameter
	    $hybridauth = $this->container['hybridAuth'];

	    // try to authenticate the user with twitter,
	    // user will be redirected to Twitter for authentication,
	    // if he already did, then Hybridauth will ignore this step and return an instance of the adapter
	    $user = $hybridauth->authenticate($provider);

	    // get the user profile
	    $user_profile = $user->getUserProfile();
	    $svc = $this->container['userService'];
	    //https://hybridauth.github.io/hybridauth/userguide/Profile_Data_User_Profile.html
	    $userx = $svc->socialLogin($provider, $user_profile);
	    if ($userx != NULL) {
		$this->set_user($userx);

		$this->session->set_flashdata('success', 'You have successfully signed in');
		return redirect('/reseller/profile', 'refresh');
	    }
	    /*

	      echo "Ohai there! U are connected with: <b>{$user->id}</b><br />";
	      echo "As: <b>{$user_profile->displayName}</b><br />";
	      echo "And your provider user identifier is: <b>{$user_profile->identifier}</b><br />";

	      // debug the user profile
	      print_r( $user_profile );

	      echo "Logging out..";
	      $user->logout(); */
	} catch (Exception $e) {
	    // Display the recived error,
	    // to know more please refer to Exceptions handling section on the userguide
	    switch ($e->getCode()) {
		case 0 : echo "Unspecified error.";
		    break;
		case 1 : echo "Hybriauth configuration error.";
		    break;
		case 2 : echo "Provider not properly configured.";
		    break;
		case 3 : echo "Unknown or disabled provider.";
		    break;
		case 4 : echo "Missing provider application credentials.";
		    break;
		case 5 : echo "Authentification failed. "
		    . "The user has canceled the authentication or the provider refused the connection.";
		    break;
		case 6 : echo "User profile request failed. Most likely the user is not connected "
		    . "to the provider and he should authenticate again.";
		    $user->logout();
		    break;
		case 7 : echo "User not connected to the provider.";
		    $user->logout();
		    break;
		case 8 : echo "Provider does not support this feature.";
		    break;
	    }

	    // well, basically your should not display this to the end user, just give him a hint and move on..
	    echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();
	}
    }

    public function gmail_login() {
	$this->hybridauth_login('Google');
    }

    public function twitter_login() {
	$this->hybridauth_login('Twitter');
    }

    public function facebook_login() {
	$this->hybridauth_login('Facebook');
    }

}
