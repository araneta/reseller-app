<?php
/**
 * Created by PhpStorm.
 * User: aldo
 * Date: 02/11/17
 * Time: 20:04
 */
// hybridauth-2.x.x/hybridauth/config.php
return array(
        // "base_url" the url that point to HybridAuth Endpoint (where index.php and config.php are found)
        "base_url" => "http://dev.funnlz.io/user/social-signup-callback",

        "providers" => array (
            // google
            "Google" => array ( // 'id' is your google client id
                "enabled" => true,
               "keys" => array ( "id" => "",
                   "secret" => "" )
            ),

         // facebook
            "Facebook" => array ( // 'id' is your facebook application id
                "enabled" => true,
                "keys" => array ( "id" => "", "secret" => "" ),
                "scope" => "email, user_about_me, user_birthday, user_hometown" // optional
            ),

         // twitter
            "Twitter" => array ( // 'key' is your twitter application consumer key
                "enabled" => true,
                "keys" => array ( "key" => "", "secret" => "" )
            )

         // and so on ...
      ),

      "debug_mode" => true ,

      // to enable logging, set 'debug_mode' to true, then provide here a path of a writable file
      "debug_file" => dirname(__FILE__)."/../logs/hybridauth.log",
    );
