<?php

use Pimple\Container;
use Core\Mappers\PdoAdapter;

$onHeroku = FALSE;
$env = getenv('ON_HEROKU');
if (!empty($env) && $env == '1') {
    $onHeroku = TRUE;
}

$container = new Container();

// define some services
$container['config'] = function ($c) {
    $upload_config = array();
    $upload_config['upload_path'] = '/media/aldo/5a89a644-1277-4713-a756-7c434a62b8a81/home/aldo/blog/public/media';
    $upload_config['allowed_types'] = 'gif|jpg|png';
    $upload_config['max_size'] = 2000;
    //$config['max_width']     = 1024;
    //$config['max_height']    = 768;

    $allconfig = ["ProductImageUpload" =>
	$upload_config
    ];

    return $allconfig;
};
$container['pdo'] = function ($c)use($onHeroku) {
    if ($onHeroku) {
	$dbstr = getenv('CLEARDB_DATABASE_URL');
	//ar_dump($dbstr);
	$dbstr = substr("$dbstr", 8);
	$dbstrarruser = explode(":", $dbstr);
	//Please don't look at these names. Yes I know that this is a little bit trash :D
	$dbstrarrhost = explode("@", $dbstrarruser[1]);
	$dbstrarrrecon = explode("?", $dbstrarrhost[1]);
	$dbstrarrport = explode("/", $dbstrarrrecon[0]);
	$dbpassword = $dbstrarrhost[0];
	$dbhost = $dbstrarrport[0];
	$dbport = $dbstrarrport[0];
	$dbuser = $dbstrarruser[0];
	$dbname = $dbstrarrport[1];
	unset($dbstrarrrecon);
	unset($dbstrarrport);
	unset($dbstrarruser);
	unset($dbstrarrhost);
	unset($dbstr);
	/*  //Uncomment this for debug reasons
	  echo $dbname . " - name<br>";
	  echo $dbhost . " - host<br>";
	  echo $dbport . " - port<br>";
	  echo $dbuser . " - user<br>";
	  echo $dbpassword . " - passwd<br>";
	 */
	$dbanfang = 'mysql:host=' . $dbhost . ';dbname=' . $dbname;
	$pdo = new PDO($dbanfang, $dbuser, $dbpassword);
	return $pdo;
    } else {
	$dsn = 'mysql:host=localhost;port=3306;dbname=resellerapp';
	$user = 'root';
	$password = 'willamette';

	$pdo = new \PDO($dsn, $user, $password);

	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
	return $pdo;
    }
};

$container['pdo_adapter'] = function ($c) {
    return new PdoAdapter($c);
};
/* servicess */
$container->register(new \Core\Services\ServiceProvider());
$container->register(new \ResellerApp\Services\ServiceProvider());

//data mappers using mysql
$container->register(new \Core\Mappers\MapperProvider());
$container->register(new \ResellerApp\Mappers\MapperProvider());

$config['IoCContainer'] = $container;
