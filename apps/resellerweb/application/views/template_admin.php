<?php
$content_header = array('meta_title' => $meta_title);
$content_menu = array('menu' => $menu);
$content_footer = array('slug' => $v);

$params = $this->session->userdata('params');
if($params!=NULL)
    $params = json_decode($params);
?>
<div id="container">
    <?php
    $this->load->view('layouts/admin/_header',$content_header);
    $this->load->view('layouts/admin/_menu',$content_menu);
    ?>

    <div id="body" class="container-fluid">
	<div class="row">
	    <div class="col-md-2">
		<?php $this->load->view('layouts/admin/_leftmenu'); ?>            
	    </div>
	    <div class="col-md-10">
		<div class="main-content">
		    <?php
		    $this->load->view($v);
		    ?>
		</div>

	    </div>
	</div>
     </div>   
    <?php
    $this->load->view('layouts/admin/_footer',$content_footer);
    ?>
</div>
