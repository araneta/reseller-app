
<nav class="navbar navbar-default">
    <div class="container-fluid">
	<div class="navbar-header"><a class="navbar-brand" href="<?php echo base_url('admin');?>">ResellerApp </a><button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button></div>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="nav navbar-nav navbar-right">
		<li class="dropdown nav-item">
		    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle" aria-hidden="true"></i> Hello <?php echo $this->session->userdata('displayName'); ?> ! <span class="caret"></span></a>
		    <ul class="dropdown-menu">
			<li class="nav-item"><a class="nav-link" href="/admin/profile">Profile</a></li>
			<li class="nav-item"><a class="nav-link" href="/admin/logout">Logout</a></li>
		    </ul>
		</li>
	    </ul>	    
	</div>
    </div>
</nav>
