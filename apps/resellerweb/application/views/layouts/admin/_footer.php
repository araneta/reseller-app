<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 text-center">
               
            </div>
            <div class="col-md-4 text-center">                
                <p>&copy;2017 All rights reserved.</p>
            </div>
            <div class="col-md-4 text-center">
                <p class="footer text-right">Page rendered in <strong>{elapsed_time}</strong> seconds. </p>
            </div>
        </div>
    </div>
</footer>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/DataTables/datatables.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/DataTables/ColReorderWithResize.js')?>"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?=base_url('assets/js/ie10-viewport-bug-workaround.js')?>"></script>
<script>
    var csfrData = {};
    csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';
    jQuery(document).ready(function($) {
        // Attach csfr data token
        $.ajaxSetup({
            data: csfrData
        });
    });
</script>

<!--<script type="text/javascript" src="<?=base_url('assets/js/main.min.js')?>"></script>-->

<?php
//for development may be its faster to separete the js files
if (isset($jsfiles) && count($jsfiles)){
    foreach ($jsfiles as $js){
        echo "<script type=\"text/javascript\" src=\"". base_url(). "assets/js/$js\" ></script>\r\n";
    }
}
?>
</html>
