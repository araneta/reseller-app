<?php
$menuitems = [
    '' => 'Dashboard',
    'order' => 'Order',
    'kategori-produk' => 'Kategori Produk',
    'produk' => 'Produk',
    'kategori-reseller' => 'Kategori Reseller',
    'reseller' => 'Reseller',
    'perusahaan' => 'Perusahaan',
    'laporan' => 'Laporan',
    'setting' => 'Setting',
];
?>
<ul class="nav nav-pills" id="left-nav">
    <?php foreach ($menuitems as $slugx=>$labelx):?>
	<li class="<?php if($slug==$slugx) echo 'active';?>"><a class="nav-link" href="/admin/<?php echo $slugx;?>"><?php echo $labelx;?></a></li>
    <?php endforeach;?>    
</ul>