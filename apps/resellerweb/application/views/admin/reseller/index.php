<div class="row" id="top-panel">
    <div class="col-md-6">
	<div class="page-header">
	    <h1 class="text-left">Daftar Reseller</h1>
	</div>
    </div>
    <div class="col-md-6">
	<ol class="breadcrumb pull-right">
	    <li><a><span>Home</span></a></li>
	    <li><a><span>Daftar Reseller</span></a></li>
	    <li><a><span> </span></a></li>
	</ol>
    </div>
</div>
<?php $this->load->view('status') ;?>
<div class="row">
    <div class="col-md-12"><a class="btn btn-success pull-right" role="button" href="/admin/reseller/add">Tambah kategori</a></div>
</div>
<div class="row">
    <div class="col-md-8">
	<form class="form-inline" id="frmSearch">
	    <div class="form-group"><label class="control-label">Cari </label>&nbsp;&nbsp;<input id="txtFilter" class="form-control" type="text" placeholder="Cari.."></div>
	    <div class="form-group"><button class="btn btn-primary active" type="button"> <i class="glyphicon glyphicon-search"></i></button></div>
	</form>
    </div>
    <div class="col-md-4">
	
    </div>
</div>
<div class="row">
    <div class="col-md-12">
	
	<div class="table-responsive">
	    <table id="tbl-kategori-reseller" class='table' width="100%">
		<thead>
		<tr>
		    <th>First Name</th>		   
		    <th>Last Name</th>		   
		    <th>Email</th>		   
		    <th>Kategori</th>		   
		    <th>Action</th>
		</tr>
		</thead>
		<tbody>

		</tbody>
	    </table>
	</div>
    </div>
</div>