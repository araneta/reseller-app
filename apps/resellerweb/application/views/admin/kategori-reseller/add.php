<div class="row" id="top-panel">
    <div class="col-md-6">
	<div class="page-header">
	    <h1 class="text-left">Input Kategori Reseller</h1>
	</div>
    </div>
    <div class="col-md-6">
	<ol class="breadcrumb pull-right">
	    <li><a href="/admin"><span>Home</span></a></li>
	    <li><a href="/admin/kategori-reseller"><span>Daftar Kategori Reseller</span></a></li>	    
	    <li><span>Input Kategori Reseller</span></li>
	</ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
	<?php echo form_open('/admin/kategori-reseller/add/'.$entity->id,'id="kategori-reseller-form" class="form-horizontal" role="form"');?>
	     <?php $this->load->view('status') ;?>

	    <div class="form-group"><label class="control-label col-md-4">Nama Kategori * </label>
		<div class="col-md-4"><input class="form-control" type="text" name="nama" required="required"value="<?php echo set_value('nama', $entity->nama); ?>" ></div>
	    </div>
	    <div class="form-group"><label class="control-label col-md-4">Tipe *</label>
		<div class="col-md-4">
		    <div class="radio"><label class="control-label"><input type="radio" name="tipe" value="1" <?php if($entity->tipe==1){echo 'checked';} ?> >Reseller dengan stok</label></div>
		    <div class="radio"><label class="control-label"><input type="radio" name="tipe" value="2" <?php if($entity->tipe==2){echo 'checked';} ?>>Reseller tanpa stok</label></div>
		</div>
	    </div>
	    <div class="form-group"><label class="control-label col-md-4">Potongan Harga* </label>
		<div class="col-md-4"><input class="form-control" type="text" id="potonganHarga" name="potonganHarga" value="<?php echo set_value('potonganHarga', $entity->potonganHarga); ?>"><span>% dari harga produk</span></div>
	    </div>
	    <div class="form-group"><label class="control-label col-md-4">Komisi * </label>
		<div class="col-md-4"><input class="form-control" type="text" id="persenKomisi" name="persenKomisi" value="<?php echo set_value('persenKomisi', $entity->persenKomisi); ?>"><span>% dari harga produk</span></div>
	    </div>
	    
	    <div class="form-group"><label class="control-label col-md-4"> </label>
		<div class="col-md-4"><button class="btn btn-success" type="submit" >Save </button></div>
	    </div>
	    <?php echo form_hidden('id', $entity->id);?>
	<?php echo form_close();?>
    </div>
</div>