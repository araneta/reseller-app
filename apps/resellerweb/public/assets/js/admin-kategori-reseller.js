
$(document).ready(function(){	
	if($('#tbl-kategori-reseller').length !== 0){		
        var dataTable = $('#tbl-kategori-reseller')
            .addClass('table table-striped table-bordered')
            .DataTable({
				"dom": 'Rlfrtip',
                "processing": true,
                "serverSide": true,
                "columns": [
                    { "data": 'nama'},               
                    { "data": 'tipe'},                                        
                    { "data": "id" }
                ],
                "aoColumnDefs": [
					 {
						"mRender": function ( data, type, row ) {
							if(row.tipe==1){
								return 'Reseller dengan stok';
							}else if(row.tipe==2){
								return 'Reseller tanpa stok';
							}
						},
						"aTargets": [1],
					},
                    {
                        "mRender": function ( data, type, row ) {
                            //console.log(row);
                            var html = '<a href="/admin/kategori-reseller/add/'+row.id+'" class="btn btn-warning">Edit</a>&nbsp;'+
                                '<button type="submit" class="btn btn-danger" data-id="'+row.id+'">Delete</button>';
                            return html;
                        },
                        "aTargets": [2],
                        width: 140,
                    },
                    
                ],
                "aaSorting": [[ 0, "asc" ]],
                "bFilter":false,
                "sPaginationType": "full_numbers",
                "sInfoEmpty": 'No entries to show',
                "sEmptyTable": "No Sources found currently, please add at least one.",
                "ajax":{
                    "url":"/admin/kategori-reseller/search-ajax",
                    "type":"POST",
                    "data":{
                        filter:function () {
                            return $('#txtFilter').val();
                        }
                    },
                    complete: function(data) {
                        bindDeleteBtn();
                    }
                },
                "fnInitComplete": function (oSettings, json) {
                    bindDeleteBtn();
                },

            });
        function bindDeleteBtn(){
            $('#tbl-kategori-reseller .btn-danger').click(function(e){
                e.preventDefault();
                if(!confirm('Are you sure you want to delete this data')){
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "/admin/kategori-reseller/delete",
                    data: {
                        id:$(this).attr('data-id')
                    },
                    success: function(msg){
                        $('#tbl-kategori-reseller').DataTable().ajax.reload();
                    }
                });
            })
        }
        $('#frmSearch').submit(function(e){
            e.preventDefault();

            dataTable.ajax.reload();

        });
	}
    // form
    if($('#kategori-reseller-form').length !== 0){
		function checkTipe(tipe){
			if (tipe == '1') {//Reseller dengan stok
				$('#potonganHarga').prop('disabled', false);
				$('#persenKomisi').prop('disabled', true);
			}else if (tipe == '2') {//Reseller tanpa stok
				$('#potonganHarga').prop('disabled', true);
				$('#persenKomisi').prop('disabled', false);
			}
		}
		checkTipe($('input[type=radio][name=tipe]:checked').val());
        $('input[type=radio][name=tipe]').change(function() {
			checkTipe(this.value);
		});
    }
});	

