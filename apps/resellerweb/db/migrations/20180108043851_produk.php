<?php


use Phinx\Migration\AbstractMigration;

class Produk extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	$table = $this->table('Produk');
	$table->addColumn('sku', 'string', ['limit' => 255])
		->addColumn('nama', 'string', ['limit' => 255])
		->addColumn('kategoriId', 'integer')
		->addForeignKey('kategoriId', 'KategoriProduk', ['id'],
                ['constraint' => 'produk_kategori'])		
		->addColumn('keterangan', 'string', ['limit' => 255, 'null'=>true])
		->addColumn('hargaPokok', 'float')
		->addColumn('ukuran', 'float')
		->addColumn('warna', 'string', ['limit' => 255, 'null'=>true])
		->addColumn('stok', 'float')
		->addColumn('stokMinimal', 'float')
		->addColumn('hargaJual', 'float')
		->addColumn('gambar', 'string', ['limit' => 255, 'null'=>true])
		->addColumn('potongan', 'float')
		->addColumn('modifiedDate', 'datetime')
		->addColumn('createdDate', 'datetime')		
		->create();
    }
}
