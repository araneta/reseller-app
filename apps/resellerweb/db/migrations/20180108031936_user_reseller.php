<?php


use Phinx\Migration\AbstractMigration;

class UserReseller extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	$table = $this->table('ResellerUser');
	$table->addColumn('alamat', 'string', ['limit' => 255])
		->addColumn('telp', 'string', ['limit' => 255])
		->addColumn('kategoriResellerId', 'integer')
		->addForeignKey('kategoriResellerId', 'KategoriReseller', ['id'],
                ['constraint' => 'reseller_user_kategori'])		
		->addColumn('username', 'string', ['limit' => 255])
		->addColumn('namaBank', 'string', ['limit' => 255])
		->addColumn('noRekening', 'string', ['limit' => 255])
		->addColumn('modifiedDate', 'datetime')
		->addColumn('createdDate', 'datetime')
		->addColumn('userId', 'integer')
		->addForeignKey('userId', 'User', ['id'],
                ['constraint' => 'reseller_user_user_id'])		
		->create();
    }
}
