<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Entities;
use Core\Entities\BaseEntity;
/**
 * Description of Perusahaan
 *
 * @author aldo
 */
class Perusahaan extends BaseEntity{
    //put your code here
    public $id;
    public $nama;
    public $alamat;
    public $telp;
    public $logo;
}
