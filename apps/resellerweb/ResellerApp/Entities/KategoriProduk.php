<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Entities;
use Core\Entities\BaseEntity;
/**
 * Description of KategoriProduk
 *
 * @author aldo
 */
class KategoriProduk extends BaseEntity{
    //put your code here
    public $id;
    public $nama;
    
    public function validate(){
	$required = ['nama',];
	$this->required($required);
	return !$this->has_error();
    }
}
