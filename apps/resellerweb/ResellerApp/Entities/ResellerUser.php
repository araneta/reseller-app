<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Entities;
use Core\Entities\User as UserEntity;

/**
 * Description of ResellerUser
 *
 * @author aldo
 */

class ResellerUser  extends UserEntity{
    public $id;
    public $roles = '{RESELLER}';
    
    public $alamat;
    public $telp;
    public $kategoriReseller;
    public $username;
    public $namaBank;
    public $noRekening;
    public $userId;


    //override
    protected function requiredNotEmpty($props){
        foreach($props as $key){
            if(!isset($this->{$key}) || (isset($this->{$key}) && empty($this->{$key}))){
                $str = preg_replace('/([a-z])([A-Z])/', '$1 $2', ucfirst($key));
                $str = str_replace('_',' ',$str);
                $this->add_error($key,$str.' tidak boleh kosong');
            }
        }
    }
    public function validate() {
	parent::validate();
	$requiredNotEmpty = ['alamat', 'telp', 'kategoriReseller', 'namaBank', 'noRekening'];
	$this->requiredNotEmpty($requiredNotEmpty);
	
	return !$this->has_error();
    }
}
