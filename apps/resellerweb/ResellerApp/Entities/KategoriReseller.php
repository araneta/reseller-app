<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Entities;
use Core\Entities\BaseEntity;
/**
 * Description of KategoriReseller
 *
 * @author aldo
 */
class KategoriReseller extends BaseEntity{
    //put your code here
    public $id;
    public $nama;
    public $tipe;
    public $persenKomisi = 0;
    public $potonganHarga = 0;
    
    public function validate(){
	$required = ['nama', 'tipe'];
	$this->required($required);
	return !$this->has_error();
    }
}
