<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Entities;
use Core\Entities\User as UserEntity;
/**
 * Description of AdminUser
 *
 * @author aldo
 */
class AdminUser extends UserEntity{
    //put your code here
    public $roles = '{ADMIN}';
    
}
