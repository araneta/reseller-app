<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Mappers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Description of MapperProvider
 *
 * @author aldo
 */
class MapperProvider implements ServiceProviderInterface {

    public function register(Container $container) {
	$container['kategoriResellerMapper'] = function($c) {
	    return new KategoriResellerMapper($c['pdo_adapter']);
	};
	$container['resellerUserMapper'] = function($c) {
	    return new ResellerUserMapper($c['pdo_adapter']);
	};
	$container['perusahaanMapper'] = function($c) {
	    return new PerusahaanMapper($c['pdo_adapter']);
	};
    }

}
