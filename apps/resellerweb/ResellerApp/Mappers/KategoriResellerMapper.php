<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Mappers;

use Core\Mappers\AbstractDataMapper;
use Core\Mappers\DatabaseAdapterInterface;
use Core\Entities\PagingResult;

use ResellerApp\Entities\KategoriReseller;
/**
 * Description of KategoriResellerMapper
 *
 * @author aldo
 */
class KategoriResellerMapper extends AbstractDataMapper {

    protected $entityTable = 'KategoriReseller';

    public function __construct(DatabaseAdapterInterface $adapter) {
	parent::__construct($adapter);
    }

    /**
     * abstract method
     */
    protected function createEntity(array $row) {
	$entity = new KategoriReseller();
	$entity->bind($row);
	return $entity;
    }

    /**
     * save kategoriReseller to db
     * @param KategoriReseller $kategoriReseller
     * return boolean
     */
    public function save(KategoriReseller &$kategoriReseller) {
	$adapter = $this->getAdapter();
	$data = [	    
	    'nama' => $kategoriReseller->nama,
	    'tipe' => $kategoriReseller->tipe,
	    'potonganHarga' => $kategoriReseller->potonganHarga,
	    'persenKomisi' => $kategoriReseller->persenKomisi,
	];
	//var_dump($data);exit(0);
	if ($kategoriReseller->id == NULL) {
	    $ret = $adapter->insert($this->entityTable, $this->setCreatedDate($data))->getLastInsertId(); //return id

	    if ($ret > 0) {
		$kategoriReseller->id = $ret;
		return TRUE;
	    }
	} else {

	    $ret = $adapter->update($this->entityTable, $this->setModifiedDate($data), 'id=' . intval($kategoriReseller->id));
	    if ($ret > 0) {
		return TRUE;
	    }
	}
	return FALSE;
    }
    public function searchKategoriReseller($paging){
	$filter = $paging->getFilter();
        $order = $paging->getSort();

        $result = new PagingResult();

        $entities = array();
        //get data
        $sql = '';
        $prm = [];
        //filter rows
        $sqlfilter = sprintf(' from %s  ', $this->entityTable);
        
        if(!empty($filter)){
            $sqlfilter .= ' where LOWER(nama) LIKE :filterx0 ';
            $prm[':filterx0'] = '%'.$filter.'%';            
        }
        $sql = $sqlfilter;
        if($order && count($order)>0){
            $sql .= " ORDER BY ";
            foreach($order as $k=>$v){
                $sql .= $k.' '.$v;
            }
            //echo $sqlfilter;
        }
        $limit = $paging->getPageSize();
        if(!empty($limit)){
            $sql .= " LIMIT " . $limit;
        }
        $offset =  $paging->getStart();
        if(!empty($offset)){
            $sql .= " OFFSET " . $offset;
        }
        //echo $sql;
	//var_dump($prm);
        $bind = NULL;
        $ret = $this->adapter->prepare('select * '.$sql)
            ->execute($prm);

        $rows = $this->adapter->fetchAll();

        if ($rows) {
            foreach ($rows as $row) {
                $entities[] = $row;
            }
            $result->setData($entities);

            //total
            $ret = $this->adapter->prepare('select count(*) as c '.$sqlfilter)
                ->execute($prm);
            $row = $this->adapter->fetch();
            if($row){
                $n = $row['c'];
            }
            $result->setTotalRecords($n);
            $result->calculate($paging);

        }

        return $result;
    }
}
