<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Mappers;

use Core\Mappers\AbstractDataMapper;
use Core\Mappers\DatabaseAdapterInterface;
use Core\Entities\PagingResult;

use ResellerApp\Entities\Perusahaan;

/**
 * Description of PerusahaanMapper
 *
 * @author aldo
 */
class PerusahaanMapper extends AbstractDataMapper {

    protected $entityTable = 'Perusahaan';

    public function __construct(DatabaseAdapterInterface $adapter) {
	parent::__construct($adapter);
    }

    /**
     * abstract method
     */
    protected function createEntity(array $row) {
	$entity = new Perusahaan();
	$entity->bind($row);
	return $entity;
    }

    /**
     * save resellerUser to db
     * @param Perusahaan $perusahaan
     * return boolean
     */
    public function save(Perusahaan &$perusahaan) {
	$adapter = $this->getAdapter();
	$data = [
	    'alamat' => $perusahaan->alamat,
	    'telp' => $perusahaan->telp,
	    'nama' => $perusahaan->nama,
	    'logo' => $perusahaan->logo,	    
	];
	//var_dump($data);exit(0);
	if ($perusahaan->id == NULL) {
	    $ret = $adapter->insert($this->entityTable, $data)->getLastInsertId(); //return id

	    if ($ret > 0) {
		$perusahaan->id = $ret;
		return TRUE;
	    }
	} else {

	    $ret = $adapter->update($this->entityTable, $data, 'id=' . intval($perusahaan->id));
	    if ($ret > 0) {
		return TRUE;
	    }
	}
	return FALSE;
    }
}