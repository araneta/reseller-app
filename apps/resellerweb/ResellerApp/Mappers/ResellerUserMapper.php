<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Mappers;

use Core\Mappers\AbstractDataMapper;
use ResellerApp\Entities\ResellerUser;

/**
 * Description of ResellerMapper
 *
 * @author aldo
 */
class ResellerUserMapper extends AbstractDataMapper {

    protected $entityTable = 'ResellerUser';

    public function __construct(DatabaseAdapterInterface $adapter) {
	parent::__construct($adapter);
    }

    /**
     * abstract method
     */
    protected function createEntity(array $row) {
	$entity = new ResellerUser();
	$entity->bind($row);
	return $entity;
    }

    /**
     * save resellerUser to db
     * @param ResellerUser $resellerUser
     * return boolean
     */
    public function save(ResellerUser &$resellerUser) {
	$adapter = $this->getAdapter();
	$data = [
	    'alamat' => $resellerUser->alamat,
	    'telp' => $resellerUser->telp,
	    'kategoriResellerId' => $resellerUser->kategoriResellerId,
	    'username' => $resellerUser->username,
	    'namaBank' => $resellerUser->namaBank,
	    'noRekening' => $resellerUser->noRekening,
	    'userId' => $resellerUser->userId,	    
	];
	//var_dump($data);exit(0);
	if ($resellerUser->id == NULL) {
	    $ret = $adapter->insert($this->entityTable, $data)->getLastInsertId(); //return id

	    if ($ret > 0) {
		$resellerUser->id = $ret;
		return TRUE;
	    }
	} else {

	    $ret = $adapter->update($this->entityTable, $data, 'id=' . intval($resellerUser->id));
	    if ($ret > 0) {
		return TRUE;
	    }
	}
	return FALSE;
    }

}
