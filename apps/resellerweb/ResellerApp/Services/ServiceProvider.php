<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
/**
 * Description of ResellerServiceProvider
 *
 * @author aldo
 */
class ServiceProvider implements ServiceProviderInterface {
    public function register(Container $container)
    {
        // register some services and parameters
        // on $pimple
	
	$container['adminService'] = function ($c) {
	    return new AdminService($c);
	};
	

    }
}
