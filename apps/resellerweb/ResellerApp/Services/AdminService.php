<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ResellerApp\Services;
use Pimple\Container;

use Core\Services\ServiceException;
use Core\Entities\DataTablesPaging;

use ResellerApp\Entities\KategoriReseller;

/**
 * kelas ini berisi semua business logic admin reseller
 *
 * @author aldo
 */
class AdminService {
    private $app;
    
    public function __construct(Container $app)
    {
        $this->app = $app;        
    }
    /*COMMAND*/
    public function saveKategoriReseller(KategoriReseller $entity){
	$mapper = $this->app['kategoriResellerMapper'];
	//validate
	if(!empty($entity->id)){
	    $exisiting = $mapper->findById($entity->id);
	    if($exisiting==NULL){
		throw new ServiceException('Item tidak ditemukan');
	    }    
	}
	
	//save to db	
	return $mapper->save($entity);
    }
    public function deleteKategoriReseller(KategoriReseller $entity){
	$mapper = $this->app['kategoriResellerMapper'];
	//validate	
	$exisiting = $mapper->findById($entity->id);
	if($exisiting==NULL){
	    throw new ServiceException('Item tidak ditemukan');
	}
	//delete from db
	return $mapper->delete($entity->id);
    }
    /*QUERY*/    
    public function searchKategoriReseller(DataTablesPaging $paging){
	$mapper = $this->app['kategoriResellerMapper'];
	return $mapper->searchKategoriReseller($paging);
    }
    
    public function findKategoriResellerById($id){
	$mapper = $this->app['kategoriResellerMapper'];
	return $mapper->findById($id);
    }
}
